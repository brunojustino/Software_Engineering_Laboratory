/** file: springmass.cpp
 ** brief: SpringMass simulation implementation
 ** author: Andrea Vedaldi
 **/

#include "springmass.h"
#include <iostream>
#include <cmath>
using namespace std;
/* ---------------------------------------------------------------- */
// class Mass
/* ---------------------------------------------------------------- */

Mass::Mass()
: position(), velocity(), force(), mass(1), radius(1)
{ }

Mass::Mass(Vector2 position, Vector2 velocity, double mass, double radius)
: position(position), velocity(velocity), force(0,0), mass(mass), radius(radius),
xmin(-1),xmax(1),ymin(-1),ymax(1)
{ }

void Mass::setForce(Vector2 f)
{
  force = f ;
}

void Mass::addForce(Vector2 f)
{
  force = force + f ;
}

Vector2 Mass::getForce() const
{
  return force ;
}

Vector2 Mass::getPosition() const
{
  return position ;
}

Vector2 Mass::getVelocity() const
{
  return this->velocity ;
}

double Mass::getRadius() const
{
  return radius ;
}

double Mass::getMass() const
{
  return mass ;
}

double Mass::getEnergy(double gravity) const
{
  double energy = 0 ;
  double h_altura = 0;

  if(this->getPosition().y < 0){
      h_altura = 1 - this->getPosition().y;
  }else
     h_altura = 1 + this->getPosition().y;
  energy = (gravity*(this->mass)*h_altura) + (0.5*(this->mass)*this->velocity.norm2()); 
  return energy ;
}

void Mass::step(double dt, double gravity)
{
  double x, y, vx, vy;
  Vector2 acceleration;
  acceleration.y = (this->getForce().y)/getMass();
  vy = this->getVelocity().y + (acceleration.y - gravity)*dt;
  y = this->getPosition().y + vy*dt + 0.5*(acceleration.y - gravity)*dt*dt;
  if(this->ymin + this->getRadius() <= y && y <= this->ymax - getRadius()){ 
    this->position.y = y;
    this->velocity.y = vy;
  }else{
    this->velocity.y = -vy;
  }

  acceleration.x = (this->getForce().x)/getMass();
  vx = this->getVelocity().x - acceleration.x*dt;
  x = this->getPosition().x + getVelocity().x*dt - 0.5*acceleration.x*dt*dt;
  if(this->xmin + this->getRadius() <= x && x <= this->xmax - this->getRadius()) { 
    this->position.x = x;
    this->velocity.x = vx;
  }else{
    if(vx>0){
      this->velocity.x = -vx;
    }
  }
}

/* ---------------------------------------------------------------- */
// class Spring
/* ---------------------------------------------------------------- */

Spring::Spring(Mass * mass1, Mass * mass2, double naturalLength, double stiffness,
 double damping): mass1(mass1), mass2(mass2), naturalLength(naturalLength), stiffness(stiffness),
 damping(damping)
{ }

Mass * Spring::getMass1() const
{
  return mass1 ;
}

Mass * Spring::getMass2() const
{
  return mass2 ;
}

Vector2 Spring::getForce() const
{
  Vector2 F;
  Vector2 u12 = mass2->getPosition() - mass1->getPosition() ;
  Vector2 unitVector = u12/getLength();
  double V = dot((mass2->getVelocity() - mass1->getVelocity()), unitVector);

  if((fabs(mass2->getPosition().y - mass1->getPosition().y) == 0) || 
    (fabs(mass1->getPosition().y - mass2->getPosition().y) == 0)){
    F.y = 0;
  } else if(mass1->getPosition().y < mass2->getPosition().y){
    F.y = 1 * this->stiffness * ((fabs(mass2->getPosition().y - mass1->getPosition().y))
     - this->naturalLength);
  } 
  else
    F.y = -1 * this->stiffness * ((fabs(mass1->getPosition().y - mass2->getPosition().y))
     - this->naturalLength);

  if(F.y < 0){
  F.y = F.y + this->damping * V;
  }
  else
    F.y = F.y - this->damping * V;


  if((fabs(mass2->getPosition().x - mass1->getPosition().x - this->naturalLength) == 0) ||
   (fabs(mass1->getPosition().x - mass2->getPosition().x - this->naturalLength) == 0))
  { 
    F.x = 0;
  }

  else if(mass1->getPosition().x < mass2->getPosition().x){
    F.x = 1 * this->stiffness * ((fabs(mass2->getPosition().x - mass1->getPosition().x))
     - this->naturalLength);
  }
  else
    F.x = -1 * this->stiffness * ((fabs(mass1->getPosition().x - mass2->getPosition().x))
     - this->naturalLength);
    return F ;
}

double Spring::getLength() const
{
  Vector2 u = mass2->getPosition() - mass1->getPosition() ;
  return u.norm()
;}

double Spring::getEnergy() const {
  double length = getLength() ;
  double dl = length - naturalLength;
  return 0.5 * stiffness * dl * dl ;
}

std::ostream& operator << (std::ostream& os, const Mass& m)
{
  os<<"("
  <<m.getPosition().x<<","
  <<m.getPosition().y<<")" ;
  return os ;
}

std::ostream& operator << (std::ostream& os, const Spring& s)
{
  return os<<"$"<<s.getLength() ;
}

/* ---------------------------------------------------------------- */
// class SpringMass : public Simulation
/* ---------------------------------------------------------------- */

SpringMass::SpringMass(double gravity)
: mass1(mass1),mass2(mass2),spring(spring),gravity(gravity)
{ 
    //mass
}

void SpringMass::display()
{
  cout<< this->mass1->getPosition().x <<" "<< mass1->getPosition().y <<"\n";
  cout<< this->mass2->getPosition().x <<" "<< mass2->getPosition().y <<"\n";
}

double SpringMass::getEnergy() const
{
  double energy = 0;
  energy = mass1->getEnergy(gravity) + mass2->getEnergy(gravity) + spring->getEnergy();
  return energy ;
}

void SpringMass::step(double dt)
{
  Vector2 g(0,-gravity);
  double energy = 0;
  double a = this->m.size();
  mass1 = (this->m[a-1]);
  mass2 = (this->m[a-2]);
  spring = sp[this->sp.size()-1];
  mass1->setForce(spring->getForce());
  mass2->setForce(-1.0*spring->getForce());
  mass1->step(dt,gravity);
  mass2->step(dt,gravity);
  energy = this->getEnergy();
}
void SpringMass::addMass(Mass *mass){
  this->m.push_back(mass);
}
void SpringMass::addSpring(Spring *s){
  this->sp.push_back(s);
}

Mass *SpringMass::getMass(int i){
  if(i >= this->m.size()) return NULL;
  else return this->m[i];
}
int SpringMass::getMassVectorLengh(){
  return this->m.size();
}