/** file: test-srpingmass.cpp
 ** brief: Tests the spring mass simulation
 ** author: Andrea Vedaldi
 **/

#include "springmass.h"
#include <iostream>
#include <stdlib.h>
using namespace std;
int main(int argc, char** argv)
{
  SpringMass springmass;
  const double mass = 0.05 ;
  const double radius = 0.02 ;
  const double naturalLength = 0.95 ;
  Mass m1(Vector2(-0.5,0.0), Vector2(), mass, radius);
  Mass m2(Vector2(0.5,0.), Vector2(), mass, radius);
  const double stiffness = 1.0;
  const double dt = 1.0/30;
  Spring spring(&m1, &m2, naturalLength, stiffness) ;
  springmass.addMass(&m1);
  springmass.addMass(&m2);
  springmass.addSpring(&spring);

  for (int i = 0 ; i < 100 ; ++i) {
    springmass.step(dt);
    springmass.display();
  }
  return 0 ;
}
